package io.renren.modules.sys.controller;

import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller公共组件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月9日 下午9:42:26
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 返回一个java.security.Principal  对象，该对象包含当前授权用户的名称
使用request.getUserPrincipal().getName()得到用户名
 
没有通过认证就没有返回值，是用户登录后才有值的，通过了JAAS认证，也就是登录
	 * @return
	 */
	protected SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}
}
