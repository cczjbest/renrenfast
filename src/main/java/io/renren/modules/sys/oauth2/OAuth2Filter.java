package io.renren.modules.sys.oauth2;

import com.google.gson.Gson;
import io.renren.common.utils.R;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * oauth2过滤器
 *控制链介绍
 * http://www.mamicode.com/info-detail-1618184.html
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-05-20 13:00
 */
public class OAuth2Filter extends AuthenticatingFilter {
//第三步
    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        //获取请求token
        String token = getRequestToken((HttpServletRequest) request);

        if(StringUtils.isBlank(token)){
            return null;
        }

        return new OAuth2Token(token);
    }

//第一步
    /**
	 * ⑤AccessControlFilter中的对onPreHandle方法做了进一步细化，
	 * isAccessAllowed方法和onAccessDenied方法达到控制效果。
	 * 这两个方法都是抽象方法，由子类去实现。到这一层应该明白。
	 * isAccessAllowed和onAccessDenied方法会影响到onPreHandle方法，
	 * 而onPreHandle方法会影响到preHandle方法，而preHandle方法会达到控制filter
	 * 链是否执行下去的效果。所以如果正在执行的filter中isAccessAllowed和onAccessDenied
	 * 都返回false，则整个filter控制链都将结束，不会到达目标方法（客户端请求的接口），
	 * 而是直接跳转到某个页面（由filter定义的，将会在authc中看到）。
	 * 
	 * 
	 * isAccessAllowed判断用户是否已登录
	 */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return false;
    }
//第二步
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        //获取请求token，如果token不存在，直接返回401
        String token = getRequestToken((HttpServletRequest) request);
        if(StringUtils.isBlank(token)){
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            String json = new Gson().toJson(R.error(HttpStatus.SC_UNAUTHORIZED, "invalid token"));
            httpResponse.getWriter().print(json);

            return false;
        }
/**
 * 可以看到提供了executeLogin方法实现用户登录的，
 * 还定义了onLoginSuccess和onLoginFailure方法，
 * 在登录成功或者失败时做一些操作
 */
        return executeLogin(request, response);
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request, ServletResponse response) {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setContentType("application/json;charset=utf-8");
        try {
            //处理登录失败的异常
            Throwable throwable = e.getCause() == null ? e : e.getCause();
            R r = R.error(HttpStatus.SC_UNAUTHORIZED, throwable.getMessage());

            String json = new Gson().toJson(r);
            httpResponse.getWriter().print(json);
        } catch (IOException e1) {

        }

        return false;
    }

    /**
     * 获取请求的token
     */
    private String getRequestToken(HttpServletRequest httpRequest){
        //从header中获取token
        String token = httpRequest.getHeader("token");

        //如果header中不存在token，则从参数中获取token
        if(StringUtils.isBlank(token)){
            token = httpRequest.getParameter("token");
        }

        return token;
    }


}
