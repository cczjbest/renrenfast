package io.renren;

import io.renren.common.utils.RedisUtils;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	@Test
	public void contextLoads() {
		SysUserEntity user = new SysUserEntity();
		user.setEmail("qqq@qq.com");
		redisUtils.set("user", user);
		//如果key相同，会更新这条数据
		user.setEmail("aaaa@qq.com");
		redisUtils.set("user", user);
		
		System.out.println(ToStringBuilder.reflectionToString(redisUtils.get("user", SysUserEntity.class)));
	}

	@Test
	public void getRedis(){
		
		String  sys=  redisUtils.get("user",-1);
		System.out.println(sys);
	}
	@Test
	public void deleteRedis(){
		redisUtils.delete("user");
		
	}
	/**
	 * 模糊查询key
	 */
	@Test
	public void selectRedis(){
		//先获取前缀为test的Key值列表。
		Set<String> keys = redisTemplate.keys("user*");
		//遍历满足条件的Key值获取对应的value值
		for (String str : keys) {
			System.out.println(redisUtils.get(str,-1));
		}
	}

}
